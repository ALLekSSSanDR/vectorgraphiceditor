﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Interface;

namespace PluginCircle
{
    public class MyCircle : IPlugin
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public MyCircle()
        {
            Size = 1;
            ColorLine = Brushes.Black.ToString();
            ColorFill = Brushes.LawnGreen.ToString();
            TypeLine = new DoubleCollection() { 1, 0 }.ToString();
            Points = new List<Point>();
            IsVisible = true;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="size">Размер линии</param>
        /// <param name="colorLine">Цвет линии</param>
        /// <param name="colorFill">Цвет заливки</param>
        /// <param name="typeLine">Тип линии</param>
        /// <param name="points">Список точек</param>
        public MyCircle(int size, string colorLine, string colorFill, string typeLine, List<Point> points)
        {
            Size = size;
            ColorLine = colorLine;
            ColorFill = colorFill;
            TypeLine = typeLine;
            Points = points;
            IsVisible = true;
        }

        /// <summary>
        /// Геттре имени плагина
        /// </summary>
        public string PluginName { get { return "Circle"; } }

        /// <summary>
        /// Геттер и сеттер размера линии
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// Геттер и сеттер цвета линии
        /// </summary>
        public string ColorLine { get; set; }

        /// <summary>
        /// Геттер и сеттер цвета заливки
        /// </summary>
        public string ColorFill { get; set; }

        /// <summary>
        /// Геттер и сеттер типа линии
        /// </summary>
        public string TypeLine { get; set; }
        /// <summary>
        /// Геттер и сеттер cписка точек
        /// </summary>
        public List<Point> Points { get; set; }
        /// <summary>
        /// Геттер и сеттер видимости линии
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Метод добавления точек 
        /// </summary>
        /// <param name="point"></param>
        public void AddPoint(Point point)
        {
            Points.Add(point);
        }

        /// <summary>
        /// Метод получения элемента Shape
        /// </summary>
        /// <returns>Обьект типа Shape</returns>
        public Shape GetDrawElement()
        {
            if (Points.Count < 2) return null;

            var myCircle = new Ellipse();
            myCircle.StrokeThickness = Size;
            myCircle.Stroke = ColorConvert(ColorLine);
            myCircle.Fill = ColorConvert(ColorFill);
            myCircle.StrokeDashArray = DoubleCollection.Parse(TypeLine);

            myCircle.Visibility = IsVisible ? Visibility.Visible : Visibility.Hidden;

            //Проверка положений координат///////////////////////////////////////////////////////////

            if (Points[0].X > Points[Points.Count - 1].X && Points[0].Y > Points[Points.Count - 1].Y)
            {
                var tmp = Points[0];
                Points[0] = Points[Points.Count - 1];
                Points[Points.Count - 1] = tmp;
            }
            if (Points[0].X < Points[Points.Count - 1].X && Points[0].Y > Points[Points.Count - 1].Y)
            {
                var tmp = Points[0];
                Points[0] = new Point(Points[0].X,Points[Points.Count-1].Y);
                Points[Points.Count - 1] = new Point(Points[Points.Count-1].X,tmp.Y);
            }
            if (Points[0].X > Points[Points.Count - 1].X && Points[0].Y < Points[Points.Count - 1].Y)
            {
                var tmp = Points[0];
                Points[0] = new Point(Points[Points.Count-1].X, Points[0].Y);
                Points[Points.Count - 1] = new Point(tmp.X, Points[Points.Count-1].Y);
            }


            
            //Первая координата
            myCircle.Margin = new Thickness(Points[0].X, Points[0].Y, 0, 0);

            //Вторая координата
            var X = Points[Points.Count - 1].X - myCircle.Margin.Left;
            var Y = Points[Points.Count - 1].Y - myCircle.Margin.Top;
            if (X <= 0) X = -X;
            if (Y <= 0) Y = -Y;

            myCircle.Height = (X + Y) / 2;
            myCircle.Width = (X + Y) / 2;

            if (myCircle.Margin.Left > Points[Points.Count - 1].X && myCircle.Margin.Top > Points[Points.Count - 1].Y)
                myCircle.Margin = new Thickness(Points[Points.Count - 1].X, Points[Points.Count - 1].Y, 0, 0);

            if (myCircle.Margin.Left > Points[Points.Count - 1].X)
                myCircle.Margin = new Thickness(Points[Points.Count - 1].X, Points[0].Y, 0, 0);

            if (myCircle.Margin.Top > Points[Points.Count - 1].Y)
                myCircle.Margin = new Thickness(Points[0].X, Points[Points.Count - 1].Y, 0, 0);

            return myCircle;
        }


        /// <summary>
        /// Функция перемещения обьекта 
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> Move(Point deltaPoint)
        {
            return new List<Point>
            {
                new Point(Points[0].X + deltaPoint.X, Points[0].Y + deltaPoint.Y),
                new Point(Points[Points.Count - 1].X + deltaPoint.X, Points[Points.Count - 1].Y + deltaPoint.Y)
            };

        }

        /// <summary>
        /// Мастабирование по верхней левой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveTopLeft(Point deltaPoint)
        {
            return new List<Point>
            {
                new Point(Points[0].X + deltaPoint.X, Points[0].Y + deltaPoint.Y),
                new Point(Points[Points.Count - 1].X,Points[Points.Count - 1].Y)
            };
        }

        /// <summary>
        /// Мастабирование по верхней правой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveTopRight(Point deltaPoint)
        {
            return new List<Point>
            {
                new Point(Points[0].X, Points[0].Y + deltaPoint.Y),
                new Point(Points[Points.Count - 1].X + deltaPoint.X, Points[Points.Count - 1].Y)
            };
        }

        /// <summary>
        /// Мастабирование по нижней левой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveBottomLeft(Point deltaPoint)
        {
            return new List<Point>
            {
                new Point(Points[0].X + deltaPoint.X, Points[0].Y),
                new Point(Points[Points.Count - 1].X, Points[Points.Count - 1].Y + deltaPoint.Y)
            };
        }

        /// <summary>
        /// Мастабирование по верхней правой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveBottomRight(Point deltaPoint)
        {
            return new List<Point>
            {
                new Point(Points[0].X, Points[0].Y),
                new Point(Points[Points.Count - 1].X + deltaPoint.X, Points[Points.Count - 1].Y + deltaPoint.Y)
            };
        }

        /// <summary>
        /// Конвертор строки в цвет
        /// </summary>
        /// <param name="color">Отьект - цвет в строковом формате</param>
        /// <returns>Цвет в формате SolidColorBrush</returns>
        public SolidColorBrush ColorConvert(string color)
        {
            var brush = (SolidColorBrush)new BrushConverter().ConvertFromString(color);
            return brush;
        }
    }
}
