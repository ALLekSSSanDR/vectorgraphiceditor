﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorGraphicEditor.BLL.Commands
{
    /// <summary>
    /// Класс-комманда отвечающий за удаление обьекта
    /// </summary>
    [Serializable]
    public class DeleteCommand:ICommand
    {
        /// <summary>
        /// Комманда
        /// </summary>
        private readonly string _commandName;

        /// <summary>
        /// Лист элементов
        /// </summary>
        private readonly List<int> _index;
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="commandName">Комманда</param>
        /// <param name="index">Лист элементов</param>
        public DeleteCommand(string commandName, List<int> index)
        {
            _commandName = commandName;
            _index = index;
        }

        /// <summary>
        /// Выполнение комманды
        /// </summary>
        /// <param name="drawManager">Обьект класса DrawManager</param>
        public void Execute(DrawManager drawManager)
        {
            drawManager.Operation(_commandName, _index);
        }
    }
}
