﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Interface;
using NSubstitute;
using NUnit.Framework;
using PluginCircle;
using PluginLine;
using PluginPolyline;
using VectorGraphicEditor.BLL.Commands;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace UnitTestProject1
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void TestConstructorLine1()
        {
            var myLine = new MyLine();
            Assert.AreEqual(Brushes.Black.ToString(), myLine.ColorLine);
        }
        [Test]
        public void TestConstructorLine2()
        {
            var myLine = new MyLine(2, Brushes.Black.ToString(), Brushes.Black.ToString(),new DoubleCollection() { 1, 0 }.ToString(), new List<Point>());
            Assert.AreEqual(2, myLine.Size);
        }
        [Test]
        public void TestConstructorCircle1()
        {
            var myCircle = new MyCircle();
            Assert.AreEqual(Brushes.Black.ToString(), myCircle.ColorLine);
        }
        [Test]
        public void TestConstructorCircle2()
        {
            var myCircle = new MyCircle(7, Brushes.Black.ToString(), Brushes.Black.ToString(), new DoubleCollection() { 1, 0 }.ToString(), new List<Point>());
            Assert.AreEqual(7, myCircle.Size);
        }

        [Test]
        public void Test1Interfaces()
        {
            var client = new Client();
            var element = new MyLine();
            element.Size = 7;

            client.Compute("NewElement",
                        element.GetType(),
                        element.Points,
                        10,
                        10,
                        element.Size, Brushes.Black, Brushes.Black, new DoubleCollection() { 1, 0 });

            Assert.AreEqual(1, client.GetDrawManager().GetListShapes().Count);
        }
    }
}
