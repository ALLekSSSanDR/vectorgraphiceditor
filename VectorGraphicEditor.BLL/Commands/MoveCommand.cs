﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace VectorGraphicEditor.BLL.Commands
{
    /// <summary>
    /// Класс-комманда отвечающий за перемещение обьекта
    /// </summary>
    [Serializable]
    public class MoveCommand : ICommand
    {
        /// <summary>
        /// Комманда
        /// </summary>
        private readonly string _commandName;

        /// <summary>
        /// Лист элементов
        /// </summary>
        private readonly List<int> _index;

        /// <summary>
        /// Координата 1
        /// </summary>
        private readonly Point _pos1;

        /// <summary>
        /// Координата 2
        /// </summary>
        private readonly Point _pos2;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="commandName">Комманда</param>
        /// <param name="index">Список элементов</param>
        /// <param name="operand1">Координата 1</param>
        /// <param name="operand2">Координата 2</param>
        public MoveCommand(string commandName, List<int> index, Point operand1, Point operand2)
        {
            _commandName = commandName;
            _index = index;
            _pos1 = operand1;
            _pos2 = operand2;

        }

        /// <summary>
        /// Выполнение комманды
        /// </summary>
        /// <param name="drawManager">Обьект класса DrawManager</param>
        public void Execute(DrawManager drawManager)
        {
            drawManager.Operation(_commandName, _index, _pos1, _pos2);
        }
    }
}
