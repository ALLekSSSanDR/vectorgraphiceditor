﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace VectorGraphicEditor.BLL
{
    /// <summary>
    /// Класс образатывающий выделение элементов
    /// </summary>
    public class SelectedElements
    {
        /// <summary>
        /// Начальная точка выделения области
        /// </summary>
        public Point _choosePoint1 { set; get; }

        /// <summary>
        /// Конечная точка выделения области
        /// </summary>
        public Point _choosePoint2 { set; get; }

        /// <summary>
        /// Список элементов
        /// </summary>
        public List<Shape> _listElement { set; get; }

        /// <summary>
        /// Список индексов элементов
        /// </summary>
        public List<int> _listIndexElements { set; get; }

        /// <summary>
        /// Флаг наличия выделенного элемента
        /// </summary>
        public bool flag { set; get; }

        /// <summary>
        /// Позиция 1
        /// </summary>
        public Point _pos1 { set; get; }

        /// <summary>
        /// Позиция 2
        /// </summary>
        public Point _pos2 { set; get; }

        /// <summary>
        /// Прямоугольник выделения
        /// </summary>
        public List<Shape> _viewRect;

        /// <summary>
        /// Расчет координат выделенного участка
        /// </summary>
        /// <param name="listShapes">Лист элементов</param>
        public void RectChange()
        {
            _listElement = new List<Shape>();
            _listIndexElements = new List<int>();

            if (_choosePoint1.X > _choosePoint2.X)
            {
                var tempX = _choosePoint1.X;
                _choosePoint1 = new Point(_choosePoint2.X, _choosePoint1.Y);
                _choosePoint2 = new Point(tempX, _choosePoint2.Y);
            }
            if (_choosePoint1.Y > _choosePoint2.Y)
            {
                var tempY = _choosePoint1.Y;
                _choosePoint1 = new Point(_choosePoint1.X, _choosePoint2.Y);
                _choosePoint2 = new Point(_choosePoint2.X, tempY);
            }
        }

        /// <summary>
        /// Получение прямогугольника и опорных точек
        /// </summary>
        /// <returns>Лист элементов - </returns>
        public List<Shape> GetRect()
        {
            _viewRect = new List<Shape>();
            var main_rect = NewPolyline(_choosePoint1.X, _choosePoint1.Y, _choosePoint2.X, _choosePoint2.Y, false);
            var rect1 = NewPolyline(_choosePoint1.X - 10, _choosePoint1.Y - 10, _choosePoint1.X + 10, _choosePoint1.Y + 10, true);
            var rect2 = NewPolyline(_choosePoint2.X - 10, _choosePoint1.Y - 10, _choosePoint2.X + 10, _choosePoint1.Y + 10, true);
            var rect3 = NewPolyline(_choosePoint2.X - 10, _choosePoint2.Y - 10, _choosePoint2.X + 10, _choosePoint2.Y + 10, true);
            var rect4 = NewPolyline(_choosePoint1.X - 10, _choosePoint2.Y - 10, _choosePoint1.X + 10, _choosePoint2.Y + 10, true);

            _viewRect.Add(main_rect);
            _viewRect.Add(rect1);
            _viewRect.Add(rect2);
            _viewRect.Add(rect3);
            _viewRect.Add(rect4);
            return _viewRect;
        }

        /// <summary>
        /// Обновление координат опорных точек
        /// </summary>
        public void UpdateRect()
        {
            LoadCoord();
            _viewRect = new List<Shape>();
            GetRect();
        }

        /// <summary>
        /// Обновление данных после выполнения комманд масштаба/перемещения
        /// </summary>
        /// <param name="newDataList"></param>
        public void UpdateDataListElements(List<Shape> newDataList)
        {
            _listElement = new List<Shape>();
            foreach (var item in _listIndexElements)
            {
                _listElement.Add(newDataList[item]);
            }
        }

        /// <summary>
        /// Получение опроных точек выделенных элементов
        /// </summary>
        /// <param name="x1">Координата </param>
        /// <param name="y1">Координата</param>
        /// <param name="x2">Координата</param>
        /// <param name="y2">Координата</param>
        /// <param name="fill">Цвет заливки</param>
        /// <returns>Элемент shape</returns>
        public Shape NewPolyline(double x1, double y1, double x2, double y2, bool fill)
        {
            var shape = new Polyline();
            shape.Stroke = Brushes.Gray;
            shape.StrokeThickness = 2;
            shape.StrokeDashArray = new DoubleCollection() { 2, 2 };
            if (fill)
            {
              //shape.Fill = Brushes.Maroon;
                shape.Stroke = Brushes.Black;
                shape.StrokeDashArray = new DoubleCollection() { 1, 0 };
            }

            shape.Points.Add(new Point(x1, y1));
            shape.Points.Add(new Point(x2, y1));
            shape.Points.Add(new Point(x2, y2));
            shape.Points.Add(new Point(x1, y2));
            shape.Points.Add(new Point(x1, y1));
            return shape;
        }

        /// <summary>
        /// Анализ координат выделенных элементов
        /// </summary>
        public void LoadCoord()
        {
            if (_listElement.Count > 0 && _listElement != null)
            {
                var x = new List<int>();
                var y = new List<int>();

                for (var i = 0; i < _listElement.Count; i++)
                {
                    switch (_listElement[i].ToString())
                    {
                        case "System.Windows.Shapes.Ellipse":
                            x.Add((int)_listElement[i].Margin.Left);
                            y.Add((int)_listElement[i].Margin.Top);
                            x.Add((int)((int)_listElement[i].Width + _listElement[i].Margin.Left));
                            y.Add((int)((int)_listElement[i].Height + _listElement[i].Margin.Top));
                            break;
                        case "System.Windows.Shapes.Line":
                            x.Add((int)(_listElement[i] as Line).X1);
                            y.Add((int)(_listElement[i] as Line).Y1);
                            x.Add((int)(_listElement[i] as Line).X2);
                            y.Add((int)(_listElement[i] as Line).Y2);
                            break;
                        case "System.Windows.Shapes.Polyline":
                            var polyline = _listElement[i] as Polyline;
                            foreach (var iteam in polyline.Points)
                            {
                                x.Add((int)iteam.X);
                                y.Add((int)iteam.Y);
                            }
                            break;
                        case "System.Windows.Shapes.Polygon":
                            var polygon = _listElement[i] as Polygon;
                            foreach (var iteam in polygon.Points)
                            {
                                x.Add((int)iteam.X);
                                y.Add((int)iteam.Y);
                            }
                            break;
                    }
                }

                x.Sort();
                y.Sort();
                _choosePoint1 = new Point(x[0], y[0]);
                _choosePoint2 = new Point(x.LastOrDefault(), y.LastOrDefault());
            }
        }

        /// <summary>
        /// Проверка нажатия на определенную опорную точку
        /// </summary>
        /// <returns></returns>
        public string CheckReck()
        {
            if (_choosePoint1.X - 10 < _pos1.X && _pos1.X < _choosePoint1.X + 10 && _choosePoint1.Y - 10 < _pos1.Y && _pos1.Y < _choosePoint1.Y + 10)
                return "one";
            if (_choosePoint2.X - 10 < _pos1.X && _pos1.X < _choosePoint2.X + 10 && _choosePoint1.Y - 10 < _pos1.Y && _pos1.Y < _choosePoint1.Y + 10)
                return "two";
            if (_choosePoint2.X - 10 < _pos1.X && _pos1.X < _choosePoint2.X + 10 && _choosePoint2.Y - 10 < _pos1.Y && _pos1.Y < _choosePoint2.Y + 10)
                return "tree";
            if (_choosePoint1.X - 10 < _pos1.X && _pos1.X < _choosePoint1.X + 10 && _choosePoint2.Y - 10 < _pos1.Y && _pos1.Y < _choosePoint2.Y + 10)
                return "four";
            if (_choosePoint1.X < _pos1.X && _pos1.X < _choosePoint2.X && _choosePoint1.Y < _pos1.Y && _pos1.Y < _choosePoint2.Y)
                return "zero";
            return "null";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static double RenderedIntersect(Visual ctx, Geometry s1, Shape s2)
        {
            var p = new Pen(Brushes.Transparent, 0.01);
            var t2 = s2.TransformToAncestor(ctx) as Transform;
            var g2 = s2.RenderedGeometry;
            if (s2 is Line) g2 = g2.GetWidenedPathGeometry(p);
            g2.Transform = t2;
            return new CombinedGeometry(GeometryCombineMode.Intersect, s1, g2).GetArea();
        }
    }
}
