﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Interface;

namespace PluginEllipse
{
    public class MyEllipse : IPlugin
    {
        /// <summary>
        /// Название плагина
        /// </summary>
        private const string _pluginName = "Ellipse";

        /// <summary>
        /// Размер линии
        /// </summary>
        private int _size;

        /// <summary>
        /// Цвет линии
        /// </summary>
        private string _colorLine;

        /// <summary>
        /// Цвет заливки 
        /// </summary>
        private string _colorFill;

        /// <summary>
        /// Тип линии
        /// </summary>
        private string _typeLine;

        /// <summary>
        /// Видимость элемента
        /// </summary>
        private bool _isVisible;
        
        /// <summary>
        /// Список опорных точек
        /// </summary>
        private List<Point> _points;

        /// <summary>
        /// Конструктор
        /// </summary>
        public MyEllipse()
        {
            _size = 1;
            _colorLine = Brushes.Black.ToString();
            _colorFill = Brushes.LawnGreen.ToString();
            _typeLine = new DoubleCollection() { 1, 0 }.ToString();
            _points = new List<Point>();
            _isVisible = true;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="size">Размер линии</param>
        /// <param name="colorLine">Цвет линии</param>
        /// <param name="colorFill">Цвет заливки</param>
        /// <param name="typeLine">Тип линии</param>
        /// <param name="points">Список точек</param>
        public MyEllipse(int size, string colorLine, string colorFill, string typeLine, List<Point> points)
        {
            _size = size;
            _colorLine = colorLine;
            _colorFill = colorFill;
            _typeLine = typeLine;
            _points = points;
            _isVisible = true;
        }

        /// <summary>
        /// Геттре имени плагина
        /// </summary>
        public string PluginName
        {
            get { return _pluginName; }
        }

        /// <summary>
        /// Геттер и сеттер размера линии
        /// </summary>
        public int Size
        {
            get { return _size; }
            set { _size = value; }
        }

        /// <summary>
        /// Геттер и сеттер цвета линии
        /// </summary>
        public string ColorLine
        {
            get { return _colorLine; }
            set { _colorLine = value; }
        }

        /// <summary>
        /// Геттер и сеттер цвета заливки
        /// </summary>
        public string ColorFill
        {
            get { return _colorFill; }
            set { _colorFill = value; }
        }

        /// <summary>
        /// Геттер и сеттер типа линии
        /// </summary>
        public string TypeLine
        {
            get { return _typeLine; }
            set { _typeLine = value; }
        }

        /// <summary>
        /// Списка точек
        /// </summary>
        public List<Point> Points
        {
            get { return _points; }
            set { _points = value; }
        }

        /// <summary>
        /// Геттер и сеттер видимости линии
        /// </summary>
        public bool IsVisible
        {
            get { return _isVisible; }
            set { _isVisible = value; }
        }

        /// <summary>
        /// Метода добавления точек 
        /// </summary>
        /// <param name="point"></param>
        public void AddPoint(Point point)
        {
            _points.Add(point);
        }

        /// <summary>
        /// Метод получения элемента Shape
        /// </summary>
        /// <returns>Обьект типа Shape</returns>
        public Shape GetDrawElement()
        {
            if (_points.Count < 2) return null;

            var myEllipse = new Ellipse();
            myEllipse.StrokeThickness = _size;
            myEllipse.Stroke = ColorConvert(_colorLine);
            myEllipse.Fill = ColorConvert(_colorFill);
            myEllipse.StrokeDashArray = DoubleCollection.Parse(_typeLine);

            myEllipse.Visibility = IsVisible ? Visibility.Visible : Visibility.Hidden;

            //Проверка положений координат///////////////////////////////////////////////////////////

            if (_points[0].X > _points[_points.Count - 1].X && _points[0].Y > _points[_points.Count - 1].Y)
            {
                var tmp = _points[0];
                _points[0] = _points[_points.Count - 1];
                _points[_points.Count - 1] = tmp;
            }
            if (_points[0].X < _points[_points.Count - 1].X && _points[0].Y > _points[_points.Count - 1].Y)
            {
                var tmp = _points[0];
                _points[0] = new Point(_points[0].X, _points[_points.Count - 1].Y);
                _points[_points.Count - 1] = new Point(_points[_points.Count - 1].X, tmp.Y);
            }
            if (_points[0].X > _points[_points.Count - 1].X && _points[0].Y < _points[_points.Count - 1].Y)
            {
                var tmp = _points[0];
                _points[0] = new Point(_points[_points.Count - 1].X, _points[0].Y);
                _points[_points.Count - 1] = new Point(tmp.X, _points[_points.Count - 1].Y);
            }
            //Первая координата
            myEllipse.Margin = new Thickness(_points[0].X, _points[0].Y, 0, 0);

             //Вторая координата
            var X = _points[_points.Count-1].X - myEllipse.Margin.Left;
            var Y = _points[_points.Count-1].Y - myEllipse.Margin.Top;
            if (X <= 0) X = -X;
            if (Y <= 0) Y = -Y;

            myEllipse.Height = Y;
            myEllipse.Width = X;

            if (myEllipse.Margin.Left > _points[_points.Count - 1].X && myEllipse.Margin.Top > _points[_points.Count - 1].Y)
                myEllipse.Margin = new Thickness(_points[_points.Count - 1].X, _points[_points.Count - 1].Y, 0, 0);

            if (myEllipse.Margin.Left > _points[_points.Count - 1].X)
                myEllipse.Margin = new Thickness(_points[_points.Count - 1].X, _points[0].Y, 0, 0);

            if (myEllipse.Margin.Top > _points[_points.Count - 1].Y)
                myEllipse.Margin = new Thickness(_points[0].X, _points[_points.Count - 1].Y, 0, 0);

            return myEllipse;
        }


        /// <summary>
        /// Функция перемещения обьекта 
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> Move(Point deltaPoint)
        {
            var newL = new List<Point>
            {
                new Point(_points[0].X + deltaPoint.X, _points[0].Y + deltaPoint.Y),
                new Point(_points[_points.Count - 1].X + deltaPoint.X, _points[_points.Count - 1].Y + deltaPoint.Y)
            };

            return newL;
        }

        /// <summary>
        /// Мастабирование по верхней левой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveTopLeft(Point deltaPoint)
        {
            var newL = new List<Point>
            {
                new Point(_points[0].X + deltaPoint.X, _points[0].Y + deltaPoint.Y),
                new Point(_points[_points.Count - 1].X,_points[_points.Count - 1].Y)
            };
            return newL;
        }

        /// <summary>
        /// Мастабирование по верхней правой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveTopRight(Point deltaPoint)
        {
            var newL = new List<Point>
            {
                new Point(_points[0].X, _points[0].Y + deltaPoint.Y),
                new Point(_points[_points.Count - 1].X + deltaPoint.X, _points[_points.Count - 1].Y)
            };
            return newL;
        }

        /// <summary>
        /// Мастабирование по нижней левой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveBottomLeft(Point deltaPoint)
        {
            var newL = new List<Point>
            {
                new Point(_points[0].X + deltaPoint.X, _points[0].Y),
                new Point(_points[_points.Count - 1].X, _points[_points.Count - 1].Y + deltaPoint.Y)
            };
            return newL;
        }

        /// <summary>
        /// Мастабирование по верхней правой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveBottomRight(Point deltaPoint)
        {
            return new List<Point>
            {
                new Point(_points[0].X, _points[0].Y),
                new Point(_points[_points.Count - 1].X + deltaPoint.X, _points[_points.Count - 1].Y + deltaPoint.Y)
            };
        }

        /// <summary>
        /// Конвертор строки в цвет
        /// </summary>
        /// <param name="color">Отьект - цвет в строковом формате</param>
        /// <returns></returns>
        public SolidColorBrush ColorConvert(string color)
        {
            var brush = (SolidColorBrush)new BrushConverter().ConvertFromString(color);
            return brush;
        }
    }
}
