﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface;
using System.Drawing;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Brushes = System.Windows.Media.Brushes;
using Point = System.Windows.Point;

namespace PluginLine
{
    public class MyLine : IPlugin
    {
        /// <summary>
        /// Название плагина
        /// </summary>
        private const string _pluginName = "Line";

        /// <summary>
        /// Размер линии
        /// </summary>
        private int _size;

        /// <summary>
        /// Цвет линии
        /// </summary>
        private string _colorLine;

        /// <summary>
        /// Цвет заливки 
        /// </summary>
        private string _colorFill;

        /// <summary>
        /// Тип линии
        /// </summary>
        private string _typeLine;

        /// <summary>
        /// Видимость элемента
        /// </summary>
        private bool _isVisible;


        /// <summary>
        /// Список опорных точек
        /// </summary>
        private List<Point> _points;

        /// <summary>
        /// Конструктор
        /// </summary>
        public MyLine()
        {
            _size = 1;
            _colorLine = Brushes.Black.ToString();
            _colorFill = Brushes.LawnGreen.ToString();
            _typeLine = new DoubleCollection() { 1, 0 }.ToString();
            _points = new List<Point>();
            _isVisible = true;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="size">Размер линии</param>
        /// <param name="colorLine">Цвет линии</param>
        /// <param name="colorFill">Цвет заливки</param>
        /// <param name="typeLine">Тип линии</param>
        /// <param name="points">Список точек</param>
        public MyLine(int size, string colorLine, string colorFill, string typeLine, List<Point> points)
        {
            _size = size;
            _colorLine = colorLine;
            _colorFill = colorFill;
            _typeLine = typeLine;
            _points = points;
            _isVisible = true;
        }

        /// <summary>
        /// Геттре имени плагина
        /// </summary>
        public string PluginName
        {
            get { return _pluginName; }
        }

        /// <summary>
        /// Геттер и сеттер размера линии
        /// </summary>
        public int Size
        {
            get { return _size; }
            set { _size = value; }
        }

        /// <summary>
        /// Геттер и сеттер цвета линии
        /// </summary>
        public string ColorLine
        {
            get { return _colorLine; }
            set { _colorLine = value; }
        }

        /// <summary>
        /// Геттер и сеттер цвета заливки
        /// </summary>
        public string ColorFill
        {
            get { return _colorFill; }
            set { _colorFill = value; }
        }

        /// <summary>
        /// Геттер и сеттер типа линии
        /// </summary>
        public string TypeLine
        {
            get { return _typeLine; }
            set { _typeLine = value; }
        }

        /// <summary>
        /// Геттер и сеттер cписка точек
        /// </summary>
        public List<Point> Points
        {
            get { return _points; }
            set { _points = value; }
        }

        /// <summary>
        /// Геттер и сеттер видимости линии
        /// </summary>
        public bool IsVisible
        {
            get { return _isVisible; }
            set { _isVisible = value; }
        }

        /// <summary>
        /// Метод добавления точек 
        /// </summary>
        /// <param name="point"></param>
        public void AddPoint(Point point)
        {
            if (point == null) return;
            _points.Add(point);
        }

        /// <summary>
        /// Метод получения элемента Shape
        /// </summary>
        /// <returns>Обьект типа Shape</returns>
        public Shape GetDrawElement()
        {
            if (_points.Count < 2) return null;

            var myLine = new Line();
            myLine.StrokeThickness = _size;
            myLine.Stroke = ColorConvert(_colorLine);
            myLine.StrokeDashArray = DoubleCollection.Parse(_typeLine);

            myLine.Visibility = IsVisible ? Visibility.Visible : Visibility.Hidden;

            myLine.X1 = _points[0].X;
            myLine.Y1 = _points[0].Y;

            myLine.X2 = _points[_points.Count - 1].X;
            myLine.Y2 = _points[_points.Count - 1].Y;

            return myLine;
        }

        /// <summary>
        /// Функция перемещения обьекта 
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> Move(Point deltaPoint)
        {
            var newL = new List<Point>
            {
                new Point(_points[0].X + deltaPoint.X, _points[0].Y + deltaPoint.Y),
                new Point(_points[_points.Count - 1].X + deltaPoint.X, _points[_points.Count - 1].Y + deltaPoint.Y)
            };
            return newL;
        }

        /// <summary>
        /// Мастабирование по верхней левой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveTopLeft(Point deltaPoint)
        {
            var newL = new List<Point>();
            var lastPointnumber = _points.Count - 1;

            if (_points[0].X < _points[lastPointnumber].X && _points[0].Y < _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X + deltaPoint.X, _points[0].Y + deltaPoint.Y));
                newL.Add(new Point(_points[lastPointnumber].X, _points[lastPointnumber].Y));
            }
            else if (_points[0].X > _points[lastPointnumber].X && _points[0].Y < _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X, _points[0].Y + deltaPoint.Y));
                newL.Add(new Point(_points[lastPointnumber].X + deltaPoint.X, _points[lastPointnumber].Y));
            }
            else if (_points[0].X > _points[lastPointnumber].X && _points[0].Y > _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X, _points[0].Y));
                newL.Add(new Point(_points[lastPointnumber].X + deltaPoint.X, _points[lastPointnumber].Y + deltaPoint.Y));
            }
            else if (_points[0].X < _points[lastPointnumber].X && _points[0].Y > _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X + deltaPoint.X, _points[0].Y));
                newL.Add(new Point(_points[lastPointnumber].X, _points[lastPointnumber].Y + deltaPoint.Y));
            }
            return newL;
        }

        /// <summary>
        /// Мастабирование по верхней правой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveTopRight(Point deltaPoint)
        {
            var newL = new List<Point>();
            var lastPointnumber = _points.Count - 1;

            if (_points[0].X < _points[lastPointnumber].X && _points[0].Y < _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X, _points[0].Y + deltaPoint.Y));
                newL.Add(new Point(_points[lastPointnumber].X + deltaPoint.X, _points[lastPointnumber].Y));
            }
            else if (_points[0].X > _points[lastPointnumber].X && _points[0].Y < _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X + deltaPoint.X, _points[0].Y + deltaPoint.Y));
                newL.Add(new Point(_points[lastPointnumber].X, _points[lastPointnumber].Y));
            }
            else if (_points[0].X > _points[lastPointnumber].X && _points[0].Y > _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X + deltaPoint.X, _points[0].Y));
                newL.Add(new Point(_points[lastPointnumber].X, _points[lastPointnumber].Y + deltaPoint.Y));
            }
            else if (_points[0].X < _points[lastPointnumber].X && _points[0].Y > _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X, _points[0].Y));
                newL.Add(new Point(_points[lastPointnumber].X + deltaPoint.X, _points[lastPointnumber].Y + deltaPoint.Y));
            }
            return newL;
        }

        /// <summary>
        /// Мастабирование по нижней левой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveBottomLeft(Point deltaPoint)
        {
            var newL = new List<Point>();
            var lastPointnumber = _points.Count - 1;

            if (_points[0].X < _points[lastPointnumber].X && _points[0].Y < _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X + deltaPoint.X, _points[0].Y));
                newL.Add(new Point(_points[lastPointnumber].X, _points[lastPointnumber].Y + deltaPoint.Y));
            }
            else if (_points[0].X > _points[lastPointnumber].X && _points[0].Y < _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X, _points[0].Y));
                newL.Add(new Point(_points[lastPointnumber].X + deltaPoint.X, _points[lastPointnumber].Y + deltaPoint.Y));
            }
            else if (_points[0].X > _points[lastPointnumber].X && _points[0].Y > _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X, _points[0].Y + deltaPoint.Y));
                newL.Add(new Point(_points[lastPointnumber].X + deltaPoint.X, _points[lastPointnumber].Y));
            }
            else if (_points[0].X < _points[lastPointnumber].X && _points[0].Y > _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X + deltaPoint.X, _points[0].Y + deltaPoint.Y));
                newL.Add(new Point(_points[lastPointnumber].X, _points[lastPointnumber].Y));
            }
            return newL;
        }

        /// <summary>
        /// Мастабирование по верхней правой опорной точке
        /// </summary>
        /// <param name="deltaPoint"></param>
        public List<Point> MoveBottomRight(Point deltaPoint)
        {
            var newL = new List<Point>();
            var lastPointnumber = _points.Count - 1;
            if (_points[0].X < _points[lastPointnumber].X && _points[0].Y < _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X, _points[0].Y));
                newL.Add(new Point(_points[lastPointnumber].X + deltaPoint.X, _points[lastPointnumber].Y + deltaPoint.Y));
            }
            else if (_points[0].X > _points[lastPointnumber].X && _points[0].Y < _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X + deltaPoint.X, _points[0].Y));
                newL.Add(new Point(_points[lastPointnumber].X, _points[lastPointnumber].Y + deltaPoint.Y));
            }
            else if (_points[0].X > _points[lastPointnumber].X && _points[0].Y > _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X + deltaPoint.X, _points[0].Y + deltaPoint.Y));
                newL.Add(new Point(_points[lastPointnumber].X, _points[lastPointnumber].Y));
            }
            else if (_points[0].X < _points[lastPointnumber].X && _points[0].Y > _points[lastPointnumber].Y)
            {
                newL.Add(new Point(_points[0].X, _points[0].Y + deltaPoint.Y));
                newL.Add(new Point(_points[lastPointnumber].X + deltaPoint.X, _points[lastPointnumber].Y));
            }
            return newL;
        }

        /// <summary>
        /// Конвертор строки в цвет
        /// </summary>
        /// <param name="color">Отьект - цвет в строковом формате</param>
        /// <returns></returns>
        public SolidColorBrush ColorConvert(string color)
        {
            var brush = (SolidColorBrush)new BrushConverter().ConvertFromString(color);
            return brush;
        }
    }
}
