﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace VectorGraphicEditor.BLL.Commands
{
    /// <summary>
    /// Класс-комманда отвечающий за изменение типа линии обьекта
    /// </summary>
    [Serializable]
    public class SetTypeCommand : ICommand
    {
        /// <summary>
        /// Команда
        /// </summary>
        private readonly string _commandName;

        /// <summary>
        /// Лист элементов
        /// </summary>
        private readonly List<int> _index;

        /// <summary>
        /// Тип линии
        /// </summary>
        private readonly string _type;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="index">Лист элементов</param>
        /// <param name="operand">Тип линии</param>
        public SetTypeCommand(string commandName, List<int> index, DoubleCollection operand)
        {
            _commandName = commandName;
            _type = operand.ToString();
            _index = index;
        }

        /// <summary>
        /// Выполнение комманды
        /// </summary>
        /// <param name="drawManager">Обьект класса DrawManager</param>
        public void Execute(DrawManager drawManager)
        {
            drawManager.Operation(_commandName, _index, DoubleCollection.Parse(_type));
        }
    }
}
