﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace VectorGraphicEditor.BLL.Commands
{
    /// <summary>
    /// Класс - комманда отвечающий за изменение толщины обьекта
    /// </summary>
    [Serializable]
    public class SetSizeLineCommand : ICommand
    {
        /// <summary>
        /// Команда
        /// </summary>
        private readonly string _commandName;

        /// <summary>
        /// Лист элементов
        /// </summary>
        private readonly List<int> _index;

        /// <summary>
        /// Толщина линии
        /// </summary>
        private readonly int _size;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="index">Лист элементов</param>
        /// <param name="operand">Толщина линии</param>
        public SetSizeLineCommand(string commandName, List<int> index, int operand)
        {
            _commandName = commandName;
            _size = operand;
            _index = index;
        }

        /// <summary>
        /// Выполнение комманды
        /// </summary>
        /// <param name="drawManager">Обьект класса DrawManager</param>
        public void Execute(DrawManager drawManager)
        {
            drawManager.Operation(_commandName, _index, _size);
        }
    }
}
