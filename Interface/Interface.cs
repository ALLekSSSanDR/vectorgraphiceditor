﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using Point = System.Windows.Point;

namespace Interface
{
    public interface IPlugin
    {
        /// <summary>
        /// Имя
        /// </summary>
        string PluginName { get; }

        /// <summary>
        /// Размер линии
        /// </summary>
        int Size { get; set; }

        /// <summary>
        /// Цвет линии
        /// </summary>
        string ColorLine { get; set; }

        /// <summary>
        /// Цвет заливки 
        /// </summary>
        string ColorFill { get; set; }

        /// <summary>
        /// Тип линии
        /// </summary>
        string TypeLine { get; set; }

        /// <summary>
        /// Список опорных точек
        /// </summary>
        List<Point> Points { get; set; }

        /// <summary>
        /// Флаг видимости фигуры
        /// </summary>
        bool IsVisible { get; set; }

        /// <summary>
        /// Начальная точка и/или последующее добавление
        /// </summary>
        void AddPoint(Point point);

        /// <summary>
        /// Создание и возвращение  
        /// </summary>
        /// <returns></returns>
        Shape GetDrawElement();

        /// <summary>
        /// Перемещение обьекта
        /// </summary>
        /// <param name="deltaPoint">Координата смещения</param>
        List<Point> Move(Point deltaPoint);

        /// <summary>
        /// Масштаб по верхней левой опорной точке
        /// </summary>
        /// <param name="deltaPoint">Координата смещения</param>
        List<Point> MoveTopLeft(Point deltaPoint);

        /// <summary>
        /// Масштаб по верхней правой опорной точке
        /// </summary>
        /// <param name="deltaPoint">Координата смещения</param>
        List<Point> MoveTopRight(Point deltaPoint);

        /// <summary>
        /// Масштаб по нижней левой опорной точке
        /// </summary>
        /// <param name="deltaPoint">Координата смещения</param>
        List<Point> MoveBottomLeft(Point deltaPoint);

        /// <summary>
        /// Масштаб по нижней правой опорной точке
        /// </summary>
        /// <param name="deltaPoint">Координата смещения</param>
        List<Point> MoveBottomRight(Point deltaPoint);
    }
}
