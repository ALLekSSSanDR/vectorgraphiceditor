﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace VectorGraphicEditor.BLL.Commands
{
    [Serializable]
    public class Client
    {
        /// <summary>
        /// Обьект класса DrawManager
        /// </summary>
        private DrawManager _drawManager = new DrawManager();

        /// <summary>
        /// Список комманд
        /// </summary>
        private List<ICommand> _commands = new List<ICommand>();

        /// <summary>
        /// Счетчик комманд
        /// </summary>
        private int _current = 0;

        /// <summary>
        ///  Обработчик команды создания нового элемента 
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="operandPoint">Лист точек</param>
        /// <param name="h">Высота</param>
        /// <param name="w">Ширина</param>
        /// <param name="operandSize">Толщина линии</param>
        /// <param name="operandColorLine">Цвет линии</param>
        /// <param name="operandColorFill">Цвет заливки</param>
        /// <param name="operandType">Тип линии</param>
        public void Compute(string commandName,Type typePlugin, List<Point> operandPoint, double h, double w, int operandSize, SolidColorBrush operandColorLine, SolidColorBrush operandColorFill, DoubleCollection operandType)
        {
            ICommand command = new NewShapeCommand(commandName,typePlugin, operandPoint, h, w, operandSize, operandColorLine, operandColorFill, operandType);
            command.Execute(_drawManager);
            ControlIndex(command);
        }

        /// <summary>
        /// Обработчик изменения цвета у элемента
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="index">Лист элементов</param>
        /// <param name="operand">цвет</param>
        public void Compute(string commandName, List<int> index, SolidColorBrush operand)
        {
            ICommand command = new SetColorCommand(commandName, index, operand);
            command.Execute(_drawManager);
            ControlIndex(command);
        }

        /// <summary>
        /// Обработчик изменения толщины линии у элемента
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="index">Лист элементов</param>
        /// <param name="operand">Толщина</param>
        public void Compute(string commandName, List<int> index, int operand)
        {
            ICommand command = new SetSizeLineCommand(commandName, index, operand);
            command.Execute(_drawManager);
            ControlIndex(command);
        }

        /// <summary>
        /// Обработчик изменения типа линии у элемента
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="index">Лист элементов</param>
        /// <param name="operand">Тип линии</param>
        public void Compute(string commandName, List<int> index, DoubleCollection operand)
        {
            ICommand command = new SetTypeCommand(commandName, index, operand);
            command.Execute(_drawManager);
            ControlIndex(command);
        }

        /// <summary>
        /// Обработчик перемещения/изменения элемента
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="index">Лист элементов</param>
        /// <param name="operand1">Точка 1</param>
        /// <param name="operand2">Точка 2</param>
        public void Compute(string commandName, List<int> index, Point operand1, Point operand2)
        {
            ICommand command = new MoveCommand(commandName, index, operand1, operand2);
            command.Execute(_drawManager);
            ControlIndex(command);
        }

        /// <summary>
        /// Обработчик удаления элемента
        /// </summary>
        /// <param name="commandName">Комманда</param>
        /// <param name="index">Лист элементов</param>
        public void Compute(string commandName, List<int> index)
        {
            ICommand command = new DeleteCommand(commandName, index);
            command.Execute(_drawManager);
            ControlIndex(command);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void ControlIndex(ICommand command)
        {
            // если "внутри undo" мы запускаем новую операцию,надо обрубать список команд, следующих после текущей,иначе undo/redo будут некорректны
            if (_current < _commands.Count)
                _commands.RemoveRange(_current, _commands.Count - _current);
            // Добавляем операцию к списку отмены
            _commands.Add(command);
            _current++;
        }

        /// <summary>
        /// Отмена события
        /// </summary>
        /// <param name="levels">Количество шагов</param>
        public void Undo(int levels)
        {
            if (_current > 0)
                _current--;
            _drawManager.Update(_commands, _current);

        }

        /// <summary>
        /// Отмена всех событии
        /// </summary>
        public void Undo()
        {
            _drawManager.Update(_commands, 0);
        }

        /// <summary>
        /// Возвращение событий
        /// </summary>
        /// <param name="levels">Количество шагов</param>
        public void Redo(int levels)
        {
            for (var i = 0; i < levels; i++)
                if (_current < _commands.Count && _current >= 0)
                    _commands[_current++].Execute(_drawManager);
        }

        public void ClearAll()
        {
            _drawManager = null;
            _drawManager = new DrawManager();
            _current = 0;
        }

        /// <summary>
        /// Гетер
        /// </summary>
        /// <returns>DrawManager</returns>
        public DrawManager GetDrawManager()
        {
            return _drawManager;
        }

        /// <summary>
        /// Геттер
        /// </summary>
        /// <returns>Лист комманд</returns>
        public List<ICommand> GetListCommands()
        {
            return _commands;
        }

        /// <summary>
        /// Сеттер комманд
        /// </summary>
        /// <param name="list">Лист комманд</param>
        public void SetListCommands(List<ICommand> list)
        {
            _commands = list;
            _current = list.Count;
            foreach (var item in list)
            {
                item.Execute(_drawManager);
            }
        }
    }
}
