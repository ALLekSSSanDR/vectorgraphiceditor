﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace VectorGraphicEditor.BLL
{
    public interface IDrawManager
    {
        void Operation(string commandName, Type typeplugin, List<Point> points, int size, SolidColorBrush colorL,
            SolidColorBrush colorF, DoubleCollection type);

        void Operation(string commandName, List<int> index, int operand);

        void Operation(string commandName, List<int> index, SolidColorBrush operand);

        void Operation(string commandName, List<int> index, DoubleCollection operand);

        void Operation(string commandName, List<int> index, Point operand1, Point operand2);

        void Operation(string commandName, List<int> index);

    }
}
