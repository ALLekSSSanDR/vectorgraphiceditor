﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Win32;

namespace VectorGraphicEditor.BLL
{
    /// <summary>
    /// Класс отвечающий за сохранения холста в бинарный формат или формат-png
    /// </summary>
    public class SaveCanvas
    {
        /// <summary>
        /// Окно сохранения файла
        /// </summary>
        private readonly SaveFileDialog _saveFileDialog;


        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="fileName">Путь файла</param>
        /// <param name="defaultExt">расширение</param>
        /// <param name="filter">Фильтр</param>
        public SaveCanvas(string fileName, string defaultExt, string filter)
        {
            _saveFileDialog = new SaveFileDialog();
            _saveFileDialog.FileName = fileName;
            _saveFileDialog.DefaultExt = defaultExt;
            _saveFileDialog.Filter = filter;
        }

        /// <summary>
        /// Функция сохранения бинарного файла (список команд)
        /// </summary>
        /// <param name="list">Список команд</param>
        public void SaveBinaryFile(List<ICommand> list )
        {
            var result = _saveFileDialog.ShowDialog();
            if (result != true) return;
            var filename = _saveFileDialog.FileName;
            //Откроем поток для записи в файл
            var fs = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            var bf = new BinaryFormatter();
            //Сериализация
            bf.Serialize(fs, list);
            fs.Close();
        }

        /// <summary>
        /// Функция сохранения графического файла
        /// </summary>
        /// <param name="canvas"></param>
        public void SaveImageFile(RenderTargetBitmap canvas)
        {
            var result = _saveFileDialog.ShowDialog();
            if (result != true) return;
            var filename = _saveFileDialog.FileName;
            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(canvas));
            //save to memory stream
            var memoryStream = new MemoryStream();
            pngEncoder.Save(memoryStream);
            memoryStream.Close();
            File.WriteAllBytes(filename, memoryStream.ToArray());
        }
    }
}
