﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VectorGraphicEditor.BLL.Commands;

namespace VectorGraphicEditor.BLL
{
    public interface ICommand
    {
        void Execute(DrawManager drawManager);
    }
}
