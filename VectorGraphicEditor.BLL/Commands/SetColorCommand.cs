﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace VectorGraphicEditor.BLL.Commands
{
    /// <summary>
    /// Класс-комманда отвечающий за испенение цета обьекта
    /// </summary>
    [Serializable]
    public class SetColorCommand:ICommand
    {
        /// <summary>
        /// Команда
        /// </summary>
        private readonly string _commandName;

        /// <summary>
        /// Лист элементов
        /// </summary>
        private readonly List<int> _index;

        /// <summary>
        /// Цвет
        /// </summary>
        private readonly string _color;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="index">Лист элементов</param>
        /// <param name="operand">Цвет</param>
        public SetColorCommand(string commandName, List<int> index, SolidColorBrush operand)
        {
            _commandName = commandName;
            _color = operand.ToString();
            _index = index;
        }

        /// <summary>
        /// Выполнение комманды
        /// </summary>
        /// <param name="drawManager">Обьект класса DrawManager</param>
        public void Execute(DrawManager drawManager)
        {
            drawManager.Operation(_commandName, _index, ColorConvert(_color));
        }

        /// <summary>
        /// Конвертер цвета
        /// </summary>
        /// <param name="color">Цвет в строковом формате</param>
        /// <returns>Цвет в формате SolidColorBrush</returns>
        public SolidColorBrush ColorConvert(string color)
        {
            var Brush = (SolidColorBrush)new BrushConverter().ConvertFromString(color);
            return Brush;
        }
    }
}
