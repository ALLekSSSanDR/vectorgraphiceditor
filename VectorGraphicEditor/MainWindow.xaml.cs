﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Interface;
using Ninject;
using VectorGraphicEditor.BLL;
using VectorGraphicEditor.BLL.Commands;
using Brushes = System.Windows.Media.Brushes;
using ICommand = VectorGraphicEditor.BLL.ICommand;
using Path = System.IO.Path;
using Point = System.Windows.Point;



namespace VectorGraphicEditor
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Поля класса

        /// <summary>
        /// Обьект класса клиент, отвечает за комманды
        /// </summary>
        private Client _client;

        /// <summary>
        /// Список выделенных элементов
        /// </summary>
        private SelectedElements _selectedElements;

        /// <summary>
        /// Список скопированных элементов
        /// </summary>
        private List<Shape> _copyElements;

        /// <summary>
        /// Флаг сохранения
        /// </summary>
        private bool _isSaved;

        /// <summary>
        /// Параметр толщина линии
        /// </summary>
        private int _size;

        /// <summary>
        /// Параметр цвет линии
        /// </summary>
        private SolidColorBrush _color;

        /// <summary>
        /// Параметр цвет заливки
        /// </summary>
        private SolidColorBrush _colorFill;

        /// <summary>
        /// Параметр тип линии
        /// </summary>
        private DoubleCollection _typeStrokeDash;

        private int _pluginNumber;

        #endregion

        private IPlugin _DrawingElement;

        List<IPlugin> _pluginsDrawElements;

        /// <summary>
        /// Инициализация Window
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            _pluginsDrawElements = new List<IPlugin>();

            _size = 1;
            _color = Brushes.Black;
            _colorFill = Brushes.Black;
            _typeStrokeDash = new DoubleCollection() { 1, 0 };
            _client = new Client();

            LoadPlugins(System.AppDomain.CurrentDomain.BaseDirectory);
            AddPluginsItems();
        }

        /// <summary>
        /// Функция обновления канвы
        /// </summary>
        private void UpdateDrawElements()
        {
            var list = _client.GetDrawManager().GetListShapes();
            can1.Children.Clear();
            foreach (var iteam in list.Where(iteam => iteam != null))
            {
                can1.Children.Add(iteam);
            }

            if (_selectedElements == null) return;
            if (!_selectedElements.flag) return;
            if (_selectedElements._listIndexElements.Count <= 0) return;
            _selectedElements.UpdateRect();
            foreach (var item in _selectedElements._viewRect)
            {
                can1.Children.Add(item);
            }
        }

        #region Обработчики мыши на канве

        /// <summary>
        /// Обработчик нажания клавишь мыши по Сanvas
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //Выбор элемента и манипуляции с ним/ними
            if (CheckButtonCursour.IsChecked != null && (bool)CheckButtonCursour.IsChecked)
            {
                if (_selectedElements == null)
                {
                    _selectedElements = new SelectedElements();
                    _selectedElements._choosePoint1 = new Point(Mouse.GetPosition(can1).X, Mouse.GetPosition(can1).Y);
                    _selectedElements.flag = false;
                }
                if(_selectedElements != null)
                {
                    _selectedElements._pos1 = Mouse.GetPosition(can1);
                }
            }
            //Создание нового элемента
            else
            {
                if (_DrawingElement == null)
                {
                    _DrawingElement = (IPlugin)Activator.CreateInstance(_pluginsDrawElements[_pluginNumber].GetType());

                   // var AppKernel = new StandardKernel(new MyNinjectModule());
                  //  _DrawingElement = AppKernel.Get<IPlugin>();

                    
                    _DrawingElement.Size = _size;
                    _DrawingElement.ColorLine = _color.ToString();
                    _DrawingElement.ColorFill = _colorFill.ToString();
                    _DrawingElement.TypeLine = _typeStrokeDash.ToString();
                }
                _DrawingElement.AddPoint(Mouse.GetPosition(can1));
            }
            UpdateDrawElements();
        }

        /// <summary>
        /// Обработчик перемещения мыши по Сanvas
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            TestTextBlock.Text = "" + Mouse.GetPosition(can1).X + " " + Mouse.GetPosition(can1).Y;
            UpdateDrawElements();
        }

        /// <summary>
        /// Обработчик отпускания клавиши мыши на Сanvas
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //Курсор
            if (CheckButtonCursour.IsChecked != null && (bool)CheckButtonCursour.IsChecked)
            {
                if (_selectedElements != null && !_selectedElements.flag)
                {
                    //Координаты выделения
                    _selectedElements._choosePoint2 = new Point( Mouse.GetPosition(can1).X, Mouse.GetPosition(can1).Y);
                    //Корректировка координат из за направления движения выделения
                    _selectedElements.RectChange();
                    // Создание обьекта геометрии для нахождения пересечений обьектов can1 с ним
                    var myRectangleGeometry = new RectangleGeometry();
                    myRectangleGeometry.Rect = new Rect(new Point(_selectedElements._choosePoint1.X, _selectedElements._choosePoint1.Y), new Point(_selectedElements._choosePoint2.X, _selectedElements._choosePoint2.Y));

                    var listDrawed = _client.GetDrawManager().GetListShapes();
                    if (listDrawed != null && listDrawed.Count > 0)
                    {
                        for (var i = 0; i < listDrawed.Count; i++)
                        {
                            var x = SelectedElements.RenderedIntersect(can1, myRectangleGeometry, (Shape)can1.Children[i]);
                            if (x > 0)
                            {
                                _selectedElements._listElement.Add(listDrawed[i]);
                                _selectedElements._listIndexElements.Add(i);
                            }
                        }
                    }
                    _selectedElements.UpdateRect();
                    _selectedElements.flag = true;
                    UpdateDrawElements();
                }
                else
                {
                    if (_selectedElements != null)
                    {
                        _selectedElements._pos2 = Mouse.GetPosition(can1);
                        //Первая точка модификации на выбранном участке
                        switch (_selectedElements.CheckReck())
                        {
                            case "one":
                                _client.Compute("Move1", _selectedElements._listIndexElements, _selectedElements._pos1, _selectedElements._pos2);
                                break;
                            case "two":
                                _client.Compute("Move2", _selectedElements._listIndexElements, _selectedElements._pos1, _selectedElements._pos2);
                                break;
                            case "tree":
                                _client.Compute("Move3", _selectedElements._listIndexElements, _selectedElements._pos1, _selectedElements._pos2);
                                break;
                            case "four":
                                _client.Compute("Move4", _selectedElements._listIndexElements, _selectedElements._pos1, _selectedElements._pos2);
                                break;
                            case "zero":
                                _client.Compute("Move", _selectedElements._listIndexElements, _selectedElements._pos1, _selectedElements._pos2);
                                break;
                        }
                        _isSaved = false;
                        _selectedElements.UpdateDataListElements(_client.GetDrawManager().GetListShapes());
                    }
                    UpdateDrawElements();
                }
            }
        }

        #endregion

        /// <summary>
        /// Обработчик нажатия клавиш
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void KeyEvents(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Enter)
            {
                _selectedElements = null;
                if (_DrawingElement != null && _DrawingElement.GetDrawElement() != null)
                {
                    _client.Compute("NewElement",
                        _DrawingElement.GetType(),
                        _DrawingElement.Points,
                        _DrawingElement.GetDrawElement().Height,
                        _DrawingElement.GetDrawElement().Width,
                        _size, _color, _colorFill, _typeStrokeDash);
                    _DrawingElement = null;
                }
            }

            if (e.Key == Key.Delete || e.Key == Key.Back)
            {
                //   if (_selectedElements == null) return;
                //   if (_selectedElements._listElement.Count == 0) return;
                _copyElements = new List<Shape>();
                _copyElements = _selectedElements._listElement;
                _client.Compute("Delete", _selectedElements._listIndexElements);
                _isSaved = false;
            }
            UpdateDrawElements();
        }

        #region Обработчики изменения параметров элементов

        /// <summary>
        /// Обработчик изменения размера линии
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void ComboBox_Selected_StrokeThickness(object sender, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)sender;
            var selectedItem = (ComboBoxItem)comboBox.SelectedItem;
            if (selectedItem.Content != null)
                _size = Convert.ToInt32(selectedItem.Content.ToString());

            if (_selectedElements == null) return;
            _isSaved = false;
            _client.Compute("SetSize", _selectedElements._listIndexElements, _size);
            UpdateDrawElements();


        }

        /// <summary>
        /// Обработчик изменения цвета линии
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void ComboBox_Selected_SolidColorBrush(object sender, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)sender;
            var selectedItem = (ComboBoxItem)comboBox.SelectedItem;
            if (selectedItem.Content != null)
            {
                switch (selectedItem.Content.ToString())
                {
                    case "Черный":
                        _color = Brushes.Black;
                        break;
                    case "Красный":
                        _color = Brushes.Red;
                        break;
                    case "Зеленый":
                        _color = Brushes.LawnGreen;
                        break;
                    case "Синий":
                        _color = Brushes.Blue;
                        break;
                    case "Желтый":
                        _color = Brushes.Gold;
                        break;
                    case "Пурпурный":
                        _color = Brushes.Purple;
                        break;
                    case "Белый":
                        _color = Brushes.White;
                        break;
                }
            }
            if (_selectedElements == null) return;
            _isSaved = false;
            _client.Compute("SetColorLine", _selectedElements._listIndexElements, _color);
            UpdateDrawElements();

        }

        /// <summary>
        /// Обработчик изменения цвета заливки
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void ComboBox_Selected_SolidColorBrushFill(object sender, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)sender;
            var selectedItem = (ComboBoxItem)comboBox.SelectedItem;
            if (selectedItem.Content != null)
            {
                switch (selectedItem.Content.ToString())
                {
                    case "Черный":
                        _colorFill = Brushes.Black;
                        break;
                    case "Красный":
                        _colorFill = Brushes.Red;
                        break;
                    case "Зеленый":
                        _colorFill = Brushes.Green;
                        break;
                    case "Синий":
                        _colorFill = Brushes.Blue;
                        break;
                    case "Желтый":
                        _colorFill = Brushes.Gold;
                        break;
                    case "Пурпурный":
                        _colorFill = Brushes.Purple;
                        break;
                    case "Белый":
                        _colorFill = Brushes.White;
                        break;
                }
            }
            if (_selectedElements == null || _selectedElements._listIndexElements.Count <= 0) return;
            _client.Compute("SetColorFill", _selectedElements._listIndexElements, _colorFill);
            _isSaved = false;
            UpdateDrawElements();

        }

        /// <summary>
        /// Обработчик изменения типа линии
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void ComboBox_Selected_Type(object sender, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)sender;
            var selectedItem = (ComboBoxItem)comboBox.SelectedItem;
            if (selectedItem.Content != null)
            {
                switch (selectedItem.Content.ToString())
                {
                    case "Тип 0":
                        _typeStrokeDash = new DoubleCollection() { 1, 0 };
                        break;
                    case "Тип 1":
                        _typeStrokeDash = new DoubleCollection() { 1, 2 };
                        break;
                    case "Тип 2":
                        _typeStrokeDash = new DoubleCollection() { 2, 1 };
                        break;
                    case "Тип 3":
                        _typeStrokeDash = new DoubleCollection() { 5, 0.2, 3, 0.2 };
                        break;
                    case "Тип 4":
                        _typeStrokeDash = new DoubleCollection() { 1, 2, 3, 4 };
                        break;
                    case "Тип 5":
                        _typeStrokeDash = new DoubleCollection() { 1, 1 };
                        break;
                    case "Тип 6":
                        _typeStrokeDash = new DoubleCollection() { 2, 2 };
                        break;
                }
            }
            if (_selectedElements == null || _selectedElements._listIndexElements.Count <= 0) return;
            _isSaved = false;
            _client.Compute("SetType", _selectedElements._listIndexElements, _typeStrokeDash);
            UpdateDrawElements();

        }

        /// <summary>
        /// Обработчик изменения области рисования
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void ComboBox_Selected_SizeСanvas(object sender, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)sender;
            var selectedItem = (ComboBoxItem)comboBox.SelectedItem;
            if (selectedItem.Content != null)
            {
                switch (selectedItem.Content.ToString())
                {
                    case "100*100":
                        can1.Width = 100;
                        can1.Height = 100;
                        break;
                    case "200*200":
                        can1.Width = 200;
                        can1.Height = 200;
                        break;
                    case "400*400":
                        can1.Width = 400;
                        can1.Height = 400;
                        break;
                    case "500*500":
                        can1.Width = 500;
                        can1.Height = 500;
                        break;
                    case "1000*1000":
                        can1.Width = 1000;
                        can1.Height = 1000;
                        break;
                    case "1366*768":
                        can1.Width = 1366;
                        can1.Height = 768;
                        break;
                    case "1920*1080":
                        can1.Width = 1920;
                        can1.Height = 1080;
                        break;
                }
            }
        }

        #endregion

        #region Обработчики управления

        /// <summary>
        /// Обработчик кнопки новый документ
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_New(object sender, ExecutedRoutedEventArgs e)
        {
            _DrawingElement = null;
            _client.Undo();
            can1.Children.Clear();
            can1.Background = Brushes.White;
            _isSaved = false;
            _client.ClearAll();
        }

        /// <summary>
        /// Обработчик загрузки изображения
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_Open(object sender, ExecutedRoutedEventArgs e)
        {
            var dl1 = new Microsoft.Win32.OpenFileDialog();
            dl1.FileName = "";
            dl1.DefaultExt = ".myBinary";
            dl1.Filter = "Image Files(*.myBinary)|*.myBinary";
            var result = dl1.ShowDialog();
            if (result != true) return;
            var filename = dl1.FileName;
            //Открываем поток для чтения
            var fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            var bf = new BinaryFormatter();
            //Десериализация
            var listCommand = (List<ICommand>)bf.Deserialize(fs);
            fs.Close();
            _client = new Client();
            _client.SetListCommands(listCommand);
            UpdateDrawElements();
            _isSaved = true;
        }

        /// <summary>
        /// Обработчик сохранения для комманды
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_Save(object sender, ExecutedRoutedEventArgs e)
        {
            Button_Save(sender, new RoutedEventArgs());
        }

        /// <summary>
        /// Обработчик сохранения для кнопки
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_Save(object sender, RoutedEventArgs e)
        {
            var save = new SaveCanvas("NewMyFile", ".myBinary", "Save myBinary(*.myBinary)|*.myBinary");
            save.SaveBinaryFile(_client.GetListCommands());
        }

        /// <summary>
        /// Обработчик сохранения изображения
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_Save_As_Image(object sender, ExecutedRoutedEventArgs e)
        {
            var rect = new Rect(can1.RenderSize);
            var renderTargetBitmap = new RenderTargetBitmap((int)rect.Right, (int)rect.Bottom, 96d, 96d, PixelFormats.Default);
            renderTargetBitmap.Render(can1);

            var save = new SaveCanvas("NewImage", ".png", "Image PNG(*.PNG)|*.PNG");
            save.SaveImageFile(renderTargetBitmap);
        }

        /// <summary>
        /// Обработчик закрытия программы
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void MainWindow1_Closing(object sender, CancelEventArgs e)
        {
            if (_isSaved) return;
            var result = MessageBox.Show("Сохранить изображение?", "Сохранение", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.Yes: Button_Save(sender, new RoutedEventArgs()); break;
                case MessageBoxResult.No: e.Cancel = false; break;
                case MessageBoxResult.Cancel: e.Cancel = true; break;
            }
        }

        /// <summary>
        /// Обработчик кнопки выход
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_Exit(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Обработчик отменения события
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_Undo(object sender, ExecutedRoutedEventArgs e)
        {
            _client.Undo(1);
            _selectedElements = null;
            UpdateDrawElements();
            _isSaved = false;
        }

        /// <summary>
        /// Обработчик возвращения события
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_Redo(object sender, ExecutedRoutedEventArgs e)
        {
            _client.Redo(1);
            UpdateDrawElements();
            _isSaved = false;
        }

        /// <summary>
        /// Обработчик вырезания элемент-а/-ов
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_Cut(object sender, ExecutedRoutedEventArgs routedEventArgs)
        {
            if (_selectedElements == null) return;
            if (_selectedElements._listElement.Count == 0) return;
            _copyElements = new List<Shape>();
            _copyElements = _selectedElements._listElement;
            _client.Compute("Delete", _selectedElements._listIndexElements);
            _selectedElements = null;
            UpdateDrawElements();
        }

        /// <summary>
        /// Обработчик копирования элемент-а/-ов
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_Copy(object sender, ExecutedRoutedEventArgs routedEventArgs)
        {
            if (_selectedElements == null) return;
            if (_selectedElements._listElement.Count == 0) return;
            _copyElements = new List<Shape>();
            _copyElements = _selectedElements._listElement;
        }

        /// <summary>
        /// Обработчик вставления элемент-а/-ов
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_Paste(object sender, ExecutedRoutedEventArgs routedEventArgs)
        {
            if (_copyElements == null) return;
            if (_copyElements.Count == 0) return;

            foreach (var item in _copyElements)
            {
                var commandName = "null";
                var points = new List<Point>();
                switch (item.ToString())
                {
                    case "System.Windows.Shapes.Ellipse":
                        commandName = "NewEllipse";
                        points.Add(new Point(item.Margin.Left, item.Margin.Top));
                        break;
                    case "System.Windows.Shapes.Line":
                        commandName = "NewLine";
                        points.Add(new Point((item as Line).X1, (item as Line).Y1));
                        points.Add(new Point((item as Line).X2, (item as Line).Y2));
                        break;
                    case "System.Windows.Shapes.Polyline":
                        commandName = "NewPolyline";
                        points = (item as Polyline).Points.ToList();
                        break;
                    case "System.Windows.Shapes.Polygon":
                        commandName = "NewPolygon";
                        points = (item as Polygon).Points.ToList();
                        break;
                }
               //_client.Compute(commandName,typeof(item), points, item.Height, item.Width, (int)item.StrokeThickness, (SolidColorBrush)item.Stroke, (SolidColorBrush)item.Fill, item.StrokeDashArray);
            }
            _selectedElements = new SelectedElements();
            _selectedElements._listElement = new List<Shape>();
            _selectedElements._listIndexElements = new List<int>();
            for (var i = 0; i < _copyElements.Count; i++)
            {
                _selectedElements._listIndexElements.Add(_client.GetDrawManager().GetListShapes().Count - (i + 1));
                _selectedElements._listElement.Add(_client.GetDrawManager().GetListShapes()[_client.GetDrawManager().GetListShapes().Count - (i + 1)]);
            }
            if (_selectedElements._listIndexElements.Count > 0)
            {
                foreach (var item in _selectedElements.GetRect())
                {
                    can1.Children.Add(item);
                }
                _selectedElements.flag = true;
            }
            _selectedElements.UpdateRect();
            UpdateDrawElements();
        }

        /// <summary>
        /// Обработчик кнопки очистить
        /// </summary>
        /// <param name="sender">Объект, который вызвал событие</param>
        /// <param name="e">Дополнительная информация обработчика</param>
        private void Button_Clear(object sender, RoutedEventArgs e)
        {
            _DrawingElement = null;
            _client.Undo();
            can1.Children.Clear();
            can1.Background = Brushes.White;
            _isSaved = false;
            _client.ClearAll();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        private void LoadPlugins(string path)
        {
            var pluginFiles = Directory.GetFiles(path, "*.dll");
            _pluginsDrawElements = new List<IPlugin>();

            foreach (var pluginPath in pluginFiles)
            {
                Type objType = null;
                try
                {
                    // пытаемся загрузить библиотеку
                    var assembly = Assembly.LoadFrom(pluginPath);
                    if (assembly != null)
                    {
                        // objType = assembly.GetType(Path.GetFileNameWithoutExtension(pluginPath) + ".ElementLogic");
                        var types =assembly.GetTypes().Where(type => type.IsClass && (type.GetInterface("IPlugin",true)) != null);

                        foreach (var type in types)
                        {
                            _pluginsDrawElements.Add((IPlugin)Activator.CreateInstance(type));
                        }
                    }
                }
                catch
                {
                    continue;
                }
                try
                {
                    if (objType != null)
                    { _pluginsDrawElements.Add((IPlugin)Activator.CreateInstance(objType)); }
                }
                catch
                {
                    continue;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddPluginsItems()
        {
            var i = 0;
            foreach (IPlugin plugin in _pluginsDrawElements)
            {
                var newBtn = new Button();
                newBtn.Name = ("btn" + i + plugin.PluginName);
                newBtn.Content = (plugin.PluginName);
                newBtn.Click += PluginButtonsClick;
                MyToolBar.Items.Insert(8 + i, newBtn);
                i++;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="evArgs"></param>
        private void PluginButtonsClick(object sender, EventArgs evArgs)
        {
            var btn = (Button)sender;
            _pluginNumber = int.Parse(btn.Name.Substring(3, 1));
        }
    }
}
