﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace VectorGraphicEditor.BLL.Commands
{
    /// <summary>
    /// Класс-команда отвечающий за отрисовку новых обьектов
    /// </summary>
    [Serializable]
    public class NewShapeCommand : ICommand
    { 
        /// <summary>
        /// Команда
        /// </summary>
        private readonly string _commandName;


        private readonly Type _typePlugin;
        /// <summary>
        /// Лист точек
        /// </summary>
        private readonly List<Point> _points;

        /// <summary>
        /// Высота
        /// </summary>
        private readonly double _height;

        /// <summary>
        /// Ширина
        /// </summary>
        private readonly double _weight;

        /// <summary>
        /// Толшина линии
        /// </summary>
        private readonly int _size;

        /// <summary>
        /// Цвет линии
        /// </summary>
        private readonly string _colorLine;

        /// <summary>
        /// Цвет заливки
        /// </summary>
        private readonly string _colorFill;

        /// <summary>
        /// Тип линии
        /// </summary>
        private readonly string _type;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="points">Лист точек</</param>
        /// <param name="height">Высота</param>
        /// <param name="weight">Ширина</param>
        /// <param name="size">Толщина линии</param>
        /// <param name="colorL">Цвет линии</param>
        /// <param name="colorF">Цвет заливки</param>
        /// <param name="type">Тип линии</param>
        public NewShapeCommand(string commandName,Type typePlugin, List<Point> points, double height, double weight, int size, SolidColorBrush colorLine, SolidColorBrush colorFill, DoubleCollection type)
        {
            _commandName = commandName;
            _points = points;
            _height = height;
            _weight = weight;
            _size = size;
            _colorLine = colorLine.ToString();
            _colorFill = colorFill.ToString();
            _type = type.ToString();
            _typePlugin = typePlugin;
        }
        
        /// <summary>
        /// Выполнение комманды
        /// </summary>
        /// <param name="drawManager">Обьект класса DrawManager</param>
        public void Execute(DrawManager drawManager)
        {
            drawManager.Operation(_commandName,_typePlugin,_points, _size,ColorConvert(_colorLine),ColorConvert(_colorFill),DoubleCollection.Parse(_type));
        }

        /// <summary>
        /// Конвертер цвета
        /// </summary>
        /// <param name="color">Цвет в строковом формате</param>
        /// <returns>Цвет в формате SolidColorBrush</returns>
        public SolidColorBrush ColorConvert(string color)
        {
            var Brush = (SolidColorBrush)new BrushConverter().ConvertFromString(color);
            return Brush;
        }
    }
}
