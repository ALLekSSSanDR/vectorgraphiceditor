﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Interface;

namespace VectorGraphicEditor.BLL
{
    /// <summary>
    /// Класс - менеджер рисования
    /// </summary>
    public class DrawManager : IDrawManager
    {
        /// <summary>
        /// Список элементов отресованных на канве
        /// </summary>
        private List<IPlugin> _listPluginElements = new List<IPlugin>();

        /// <summary>
        /// Обработчик команды создания нового элемента
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="typeplugin">Тип создоваемого элемента, подключенного плагином</param>
        /// <param name="points">Лист точек</</param>
        /// <param name="size">Толщина линии</param>
        /// <param name="colorL">Цвет линии</param>
        /// <param name="colorF">Цвет заливки</param>
        /// <param name="type">Тип линии</param>
        public void Operation(string commandName,Type typeplugin, List<Point> points, int size, SolidColorBrush colorL, SolidColorBrush colorF, DoubleCollection type)
        {
            var newDrawElement = (IPlugin)Activator.CreateInstance(typeplugin);

            newDrawElement.Size = size;
            newDrawElement.ColorLine = colorL.ToString();
            newDrawElement.ColorFill = colorF.ToString();
            newDrawElement.TypeLine = type.ToString();
            newDrawElement.Points = points;

            _listPluginElements.Add(newDrawElement);
        }

        /// <summary>
        /// Обработчик изменения толщины линии у элемента
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="index">Лист элементов</param>
        /// <param name="operand">Толщина</param>
        public void Operation(string commandName, List<int> index, int operand)
        {
            foreach (var item in index)
            {
                switch (commandName)
                {
                    case "SetSize":
                        _listPluginElements[item].Size = operand;
                        break;
                }
            }
        }

        /// <summary>
        /// Обработчик изменения цвета у элемента
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="index">Лист элементов</param>
        /// <param name="operand">цвет</param>
        public void Operation(string commandName, List<int> index, SolidColorBrush operand)
        {
            foreach (var t in index)
            {
                switch (commandName)
                {
                    case "SetColorLine":
                        _listPluginElements[t].ColorLine = operand.ToString();
                        break;
                    case "SetColorFill":
                        _listPluginElements[t].ColorFill = operand.ToString();
                        break;
                }
            }
        }

        /// <summary>
        /// Обработчик изменения типа линии у элемента
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="index">Лист элементов</param>
        /// <param name="operand">Тип линии</param>
        public void Operation(string commandName, List<int> index, DoubleCollection operand)
        {
            foreach (var t in index)
            {
                switch (commandName)
                {
                    case "SetType":
                        _listPluginElements[t].TypeLine = operand.ToString();
                        break;
                }
            }
        }

        /// <summary>
        /// Обработчик перемещения/изменения элемента
        /// </summary>
        /// <param name="commandName">Команда</param>
        /// <param name="index">Лист элементов</param>
        /// <param name="operand1">Точка 1</param>
        /// <param name="operand2">Точка 2</param>
        public void Operation(string commandName, List<int> index, Point operand1, Point operand2)
        {
            var dP = new Point(operand2.X - operand1.X, operand2.Y - operand1.Y);

           // foreach (var t in index)
            for (var i = 0; i < index.Count; i++)
            {
                var element = _listPluginElements[index[i]];

                switch (commandName)
                {
                    case "Move":
                        element.Points =  element.Move(dP);
                        break;
                    case "Move1":
                        element.Points = element.MoveTopLeft(dP);
                        break;
                    case "Move2":
                        element.Points = element.MoveTopRight(dP);
                        break;
                    case "Move4":
                        element.Points = element.MoveBottomLeft(dP);
                        break;
                    case "Move3":
                        element.Points = element.MoveBottomRight(dP);
                        break;
                }
            }
        }

        /// <summary>
        /// Обработчик удаления элемента
        /// </summary>
        /// <param name="commandName">Комманда</param>
        /// <param name="index">Лист элементов</param>
        public void Operation(string commandName, List<int> index)
        {
            foreach (var t in index)
            {
                switch (commandName)
                {
                    case "Delete":
                        _listPluginElements[t].IsVisible = false;
                        break;
                }
            }
        }

        /// <summary>
        /// Геттер
        /// </summary>
        /// <returns>Список элементов канвы</returns>
        public List<Shape> GetListShapes()
        {
            var tempShapeList = new List<Shape>();
            foreach (var shape in _listPluginElements)
            {
                tempShapeList.Add(shape.GetDrawElement());
            }
            return tempShapeList;
        }

        /// <summary>
        /// Отмена события
        /// </summary>
        public void Undo()
        {
            if (_listPluginElements.Count > 0)
                _listPluginElements.RemoveAt(_listPluginElements.Count - 1);
        }

        /// <summary>
        ///  Обновление сомманд
        /// </summary>
        /// <param name="commands">Комманда</param>
        /// <param name="count">Количество отпераций</param>
        public void Update(List<ICommand> commands, int count)
        {
            _listPluginElements = null;
            _listPluginElements = new List<IPlugin>();
            for (var i = 0; i < count; i++)
            {
                commands[i].Execute(this);
            }
        }
    }
}
